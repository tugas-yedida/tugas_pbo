package com.example.yedida;

public class LuasSegitiga {
    public static void main(String[] args) {
        double alas = 6;
        double tinggi = 7;
        double luas = alas * tinggi / 2;
        System.out.println("Luas segitiga dengan alas 6 dan tinggi 7 adalah " + luas);
    }
}