package com.example.yedida;

public class VolumeTabung {
    public static void main(String[] args) {
        double jariJari = 5 / 2.0;
        double tinggi = 10;
        double volume = Math.PI * jariJari * jariJari * tinggi;
        System.out.println("Volume tabung dengan jari-jari 2.5 dan tinggi 10 adalah " + volume);
    }
}