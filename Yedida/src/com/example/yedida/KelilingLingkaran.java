package com.example.yedida;

public class KelilingLingkaran {
    public static void main(String[] args) {
        double diameter = 10;
        double jariJari = diameter / 2;
        double keliling = 2 * Math.PI * jariJari;
        System.out.println("Keliling lingkaran dengan diameter 10 adalah " + keliling);
    }
}